
REALIZADO:
Desplegar un loadbalancer hacia 2 pods que son replicas de una misma imagen de docker.

MAQUINA PARA EL SERVIDOR DE PRUEBAS:
Maquina de tier gratuita de AWS con el sistema operativo ubuntu.
Cluster en google cloud.

SUGERENCIA: 
Ver la referencia antes de ejecutar comandos para comprender mejor que hice en cada paso.

PROCEDIMIENTO DESCRITO A CONTINUACIÓN:
1. Que la maquina donde tengamos el gitlab runner pueda ejecutar kubectl en nuestro cluster
2. Descargar las imagenes de Docker desde el gitlab runner
3. Preparar un loadbalancer que apunte a 2 replicas de una imagen

1 --INSTALAR GITLAB RUNNER 

Cuando instalamos gitlab runner en una maquina también se crea el usuario "gitlab-runner" que es el usuario que ejecutará
todo el pipeline en la máquina. 

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

--El executor que utilice es del tipo "shell"

INSTALL DOCKER 
https://docs.docker.com/engine/install/ubuntu/

--Quitarle la necesidad de contraseña al usuario gitlab-runner 
Esto es únicamente para que podamos hacer pruebas nosotros como el usuario "gitlab-runner", puesto que debemos cambiar del usuario "ubuntu".
Yo lo hice en ubuntu, aquí al cambiar de un usuario a otro, aunque el otro no requiera contraseña, ubuntu va a exigir una contraseña y 
al no coincidir va a dar error entonces debemos quitarle la obligación de ingresar una contraseña al usuario "ubuntu" al momento de 
querer cambiar al usuario "gitlab-runner".

sudo nano /etc/pam.d/su
auth       [success=ignore default=1] pam_succeed_if.so user = gitlab-runner
auth       sufficient   pam_succeed_if.so use_uid user ingroup gitlab-runner
sudo usermod -aG gitlab-runner ubuntu
su - gitlab-runner

auth       [success=ignore default=1] pam_succeed_if.so user = gitlab-runner
auth       sufficient   pam_succeed_if.so use_uid user ingroup gitlab-runner
sudo usermod -aG gitlab-runner ubuntu
su - gitlab-runner

le damos permisos de sudo 
sudo usermod -aG gitlab-runner $USER
newgrp gitlab-runner

REF 
https://www.tecmint.com/switch-user-account-without-password/

-- le permitimos ejecutar sudo sin password 
Necesitamos que el usuario gitlab runner pueda ejecutar sudo y no deba ingresar la contraseña cada vez que lo hace.

Accedemos al archivo de configuraciones y le damos permisos de sudo y que pueda ejecutar sudo sin password

Entramos al archivo:

 sudo visudo

Mi configuracion resultante:

#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL
gitlab-runner ALL=(ALL:ALL) ALL
# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d
gitlab-runner ALL=(ALL:ALL) NOPASSWD:ALL

REF 
https://slimbook.es/tutoriales/linux/86-anadir-usuario-al-fichero-sudoers

Recomendación: Realizar las instalaciones con el usuario gitlab-runner

INSTALAR GCLOUD Y KUBECTL
https://cloud.google.com/sdk/docs/install#deb

INSTALAR KUBECTL (solo kubectl, otra opción)
https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/


Conectamos el usuario gitlab-runner al cluster, este comando lo obtienes en la opción "Conectar" del cluster.


USANDO KUBERENTES: 
--Para probar imagenes puedes:
kubectl run front1 --image=dockerhubayd2usac2s2021/front1 --port:4000
kubectl run front2 --image=dockerhubayd2usac2s2021/front2 --port:4001
kubectl get pods
kubectl expose pod
kubectl get services 

REFS
https://www.youtube.com/watch?v=Najm4d4hy4w
https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/


Para hacer uso de un loadbalancer con una ip externa:
en un archivo .yaml, en este caso yo le llame:  load-balancer-exampleV2.yaml
-------------------------------------
apiVersion: apps/v1
kind: Deployment
metadata:
  name: load-balancer-examplev2
  labels:
    app.kubernetes.io/name: load-balancer-examplev2
spec:
  replicas: 2
  selector:
    matchLabels:
        app.kubernetes.io/name: load-balancer-examplev2
  template:
    metadata:
      labels:
          app.kubernetes.io/name: load-balancer-examplev2
    spec:
      containers:
      - image: dockerhubayd2usac2s2021/front1
        name: frontbalancer1
        ports:
        - containerPort: 8091
----------------------------------------     


lo ejecutamos 
kubectl apply -f load-balancer-exampleV2.yaml

para revisar el deploy que acabamos de ralizar, cuando lo acabas de crear te devuelve el nombre completo del recurso para que puedas consultarlo, en mi caso fue:

kubectl get deployment.apps/load-balancer-example

igualmente puedes revisar los servicios con el comando: 
kubectl get svc

kubectl expose deployment load-balancer-examplev2 --type=LoadBalancer --name=my-servicev2

REF 
https://www.magalix.com/blog/nodejs-app-sample-from-docker-to-kubernetes-cluster


