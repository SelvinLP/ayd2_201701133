import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepracticaComponent } from './practica/homepractica/homepractica.component';
import { Page404Component } from './shared/page404/page404.component';

const routes: Routes = [
  { path: '', component: HomepracticaComponent },
  { path: 'hola', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule)},
  { path: 'client', loadChildren: () => import('./client/client.module').then( m => m.ClientModule)},
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
