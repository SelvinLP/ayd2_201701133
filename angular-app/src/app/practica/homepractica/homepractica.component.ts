import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepractica',
  templateUrl: './homepractica.component.html',
  styleUrls: ['./homepractica.component.css']
})
export class HomepracticaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

    //ESTUDIANTE
    carnet = "";
    cui= "";
    nombreusuario = "";
    correo = "";
    fecha = "";
    listestudiantes = [
      {
        carnet: "",
        cui: "",
        nombre: "",
        correo: "",
        fechanac: ""
      }
    ];
  
    //CURSO
    codigo = "";
    nombrecurso = "";
    credn = "";
    credo = "";
    listcursos = [
      {
        nombre: "",
        credn: "",
        codigo: "",
        credo: ""
      }
    ];
  
    newuser(){
      this.listestudiantes.push({
        carnet: this.carnet,
        cui: this.cui,
        nombre: this.nombreusuario,
        correo: this.correo,
        fechanac: this.fecha
      });
    }
  
    newcourse(){
      this.listcursos.push({
        nombre: this.nombrecurso,
        credn: this.credn,
        codigo: this.codigo,
        credo: this.credo
      });
      
    }
}
