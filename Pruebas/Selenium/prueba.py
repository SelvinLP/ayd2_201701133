import random
import string
import time

SELENIUM_HUB = 'http://34.125.12.115:4444/wd/hub'
#SELVIN LISANDRO ARAGON PEREZ 201701133

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

driver = webdriver
driver = webdriver.Remote(
    command_executor=SELENIUM_HUB,
    desired_capabilities=DesiredCapabilities.FIREFOX,
)

def generador_txt(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

PATH_BASE = 'http://34.122.6.133:80'

try:
    
    #Resgistro de Usuario
    #SELVIN LISANDRO ARAGON PEREZ 201701133
    urlprueba = PATH_BASE + ''
    driver.get(urlprueba)
    search_e = driver.find_element_by_name("carnet")
    search_e.clear()
    search_e.send_keys(generador_txt())

    search_w = driver.find_element_by_name("cui")
    search_w.clear()
    search_w.send_keys(generador_txt())
    
    search_t = driver.find_element_by_name("nombreusuario")
    search_t.clear()
    search_t.send_keys(generador_txt())

    search_t = driver.find_element_by_name("correo")
    search_t.clear()
    search_t.send_keys(generador_txt()+"@gmail.com")

    search_t = driver.find_element_by_name("fecha")
    search_t.clear()
    search_t.send_keys("5/11/2021")
    
    search_btn = driver.find_element_by_id("botoncrearusus")
    search_btn.click()
    time.sleep(30)
    assert "No results found." not in driver.page_source
    print("1.   Prueba de Creacion de usuario correcta")
    
    #Resgistro de Curso Correcta
    urlprueba = PATH_BASE + ''
    driver.get(urlprueba)
    search_e = driver.find_element_by_name("codigo")
    search_e.clear()
    search_e.send_keys(generador_txt())

    search_w = driver.find_element_by_name("nombrecurso")
    search_w.clear()
    search_w.send_keys(generador_txt())
    
    search_t = driver.find_element_by_name("credn")
    search_t.clear()
    search_t.send_keys(generador_txt())

    search_t = driver.find_element_by_name("credo")
    search_t.clear()
    search_t.send_keys(generador_txt())
    
    search_btn = driver.find_element_by_id("botoncrearcurso")
    search_btn.click()
    time.sleep(1)
    assert "No results found." not in driver.page_source
    print("2.   Prueba de Creacion de Curso")

except:
    print("Error en un prueba")
    assert "No results found."  in driver.page_source
finally:
    driver.quit()
    print("-- Cierra sesion del driver --")